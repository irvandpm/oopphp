<?php 

class Animal {
	public  $name,
			$legs = 2,
			$cold_blooded = 'false';

	public function __construct ($name = "nama"){
		$this->name = $name ;
		// $this->legs = $legs;
		// $this->cold_blooded = $cold_blooded;		

	}

	public function getName () {
		return $this->name;
	}

	public function getLegs () {
		return $this->legs;
	}

	public function getCold_blooded () {
		return $this->cold_blooded;
	}


}

?>