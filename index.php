<?php 
require_once 'Animal.php';
require_once 'Frog.php';
require_once 'Ape.php';


$sheep = new Animal("shaun");

// echo $sheep->name; // "shaun"
// echo $sheep->legs; // 2
// echo $sheep->cold_blooded; // false
// echo "<br>";


echo "Namanya: {$sheep->getName()} <br>"; // "shaun"
echo "Jumlah leg: {$sheep->getLegs()} <br>"; // 2
echo "Cold Blooded?: {$sheep->getCold_blooded()} <br>"; // false
echo "<hr>";

$sungokong = new Ape("kera sakti <br>");
echo $sungokong-> getName();
$sungokong->yell(); // "Auooo"
echo "<br><br>";

$kodok = new Frog("buduk");
// echo $kodok->getLegs(); //4
// echo "<br>";
echo $kodok-> getName();
echo "<br>";
$kodok->jump() ; // "hop hop"


?>